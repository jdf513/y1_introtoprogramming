\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jdf_labscript}[2016/07/27 Lab script format by JDF for UoY Dept. of Electronics]

\LoadClass[12pt]{article}


\usepackage[utf8]{inputenc}
\usepackage{parskip}
\usepackage[UKenglish]{babel}
\usepackage[margin=2cm]{geometry}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{mdframed}
\usepackage{pbox}
\usepackage{pifont}
\usepackage{xcolor}

\def\docname#1{\def\@docname{#1}}
\newcommand{\printdocname}{\@docname}

\def\modulename#1{\def\@modulename{#1}}
\newcommand{\printmodulename}{\@modulename}


\definecolor{formalshade}{rgb}{0.95,0.95,1}
\definecolor{mygreen}{RGB}{0,127,0}
\definecolor{mygray}{RGB}{100,100,100}
\definecolor{mymauve}{RGB}{100,32,255}
\definecolor{lgray}{RGB}{230,230,230}
\lstset{ %
  frame=tb,
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=t,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
%  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=python,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=4,                       % sets default tabsize to 2 spaces
  aboveskip=3mm,
  belowskip=3mm,
}


\newmdenv[
  skipabove=\topskip,
  skipbelow=\topskip,
  innermargin=0pt,
  outermargin=0pt,
  innerleftmargin=4pt,
  innerrightmargin=4pt,
  innertopmargin=2pt,
  innerbottommargin=2pt,
  backgroundcolor=formalshade,
  topline=false,
  rightline=false,
  bottomline=false,
  linecolor=green,
  linewidth=2pt,
   ]{tipbox*}
\newenvironment{tipbox}[1][43]
  {\begin{tipbox*}
   \makebox[0pt][r]{\smash{\raisebox{-.333\height}{\large\ding{#1}\hspace{10pt}}}}\ignorespaces}
  {\end{tipbox*}}

  
  
\newcommand{\task}{\makebox[0pt][r]{\smash{\raisebox{-.333\height}{\Huge\ding{45}\hspace{10pt}}}}\ignorespaces}


\newenvironment{warning}[1][43]
  {\begin{tipbox*}
   \makebox[0pt][r]{\smash{{\Huge{\fontencoding{U}\fontfamily{futs}\selectfont\char 66\relax}\hspace{10pt}}}}\ignorespaces
   %
   {\Large\textbf{Important}}\\\vspace{2pt}}
  {\end{tipbox*}}


\newenvironment{aimslist}{
\vspace{10pt}
\begin{center}
\rule{0.85\textwidth}{0.4pt}
\begin{minipage}{0.8\textwidth}
\vspace{7pt}
\textbf{\large Aims for this lab script}
\vspace{5pt}

By the end of this lab script you should know: %TODO: complete this
\vspace{5pt}
\begin{itemize}
}
{
\end{itemize}
\vspace{5pt}
\end{minipage}
\rule{0.85\textwidth}{0.4pt}
\end{center}
}

\pagestyle{fancy} 
\lhead{\printdocname} 
\rhead{\printmodulename}
\title{\huge{\printdocname} \\ \textsc{\large{\printmodulename}\\{\normalsize{Department of Electronics, University of York}}}}
\author{\vspace{-0.5cm}\small{Joel Fergusson, joel.fergusson@york.ac.uk}}
